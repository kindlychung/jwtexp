# jwtexp

Print the expiration date (in UTC) of the JWT token. See `jwtexp -h` for usage instructions.
